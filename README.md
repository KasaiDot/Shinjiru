# Shinjiru #

## Description ##

Shinjiru is anime list management tool built with Qt designed to automatically update your progress to AniList and act an desktop manager for AniList.

## Installation ##

Shinjiru's [website](http://app.shinjiru.me/index.php) currently hosts Windows builds [here](http://app.shinjiru.me/latest.php).  
For other build needs feel free to [build](#Building) Shinjiru for yourself.


## Building ##

**Build**

```
git clone git://github.com/Kazakuri/Shinjiru.git
cd Shinjiru
git submodule update --init
qmake
make
```

**Dependencies**

Shinjiru requires a few dependencies to compile and work properly, fortunately they are relatively easy to setup.

- **Qt**         - [http://www.qt.io/download-open-source/](http://www.qt.io/download-open-source/)
- **OpenSSL**    - [http://www.openssl.org/](http://www.openssl.org/)
- **QtAwesome**  - [https://github.com/Kazakuri/QtAwesome](https://github.com/Kazakuri/QtAwesome)
- **AniListAPI** - [https://github.com/Kazakuri/AniListAPI](https://github.com/Kazakuri/AniListAPI)
- **Anitomy**    - [https://github.com/erengy/anitomy](https://github.com/erengy/anitomy)
- **Fervor _autoupdate branch_**     - [https://github.com/Kazakuri/fervor](https://github.com/Kazakuri/fervor) 

## Contribute ##

If you want to contribute to Shinjiru feel free to create a pull request or an issue with suggestions/issues/features etc.

Make sure your code compiles if you're contributing.  
I don't have insanely high standards for code style but try to keep it within the [Google Style Guide](http://google-styleguide.googlecode.com/svn/trunk/cppguide.html), or at the very least, 0 errors while running the file through cpplint.py.


## Contact ##

- **Email**: <admin@shinjiru.me>
- **Website**: [http://app.shinjiru.me](http://app.shinjiru.me)
- **GitHub**: [https://github.com/Kazakuri/Shinjiru](https://github.com/Kazakuri/Shinjiru)
