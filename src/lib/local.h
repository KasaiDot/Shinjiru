/* Copyright 2015 Kazakuri */

#ifndef SRC_LIB_LOCAL_H_
#define SRC_LIB_LOCAL_H_

#include <QObject>
#include <QSqlDatabase>
#include <QCoreApplication>

#include "../api/anime.h"

class Local : public QObject {
  Q_OBJECT
 public:
  explicit Local(QObject *parent = 0);
  ~Local();

  void loadUser();
  void saveUser();

  bool loadAnime(Anime *a);
  void saveAnime(Anime *a);

 private:
  QSqlDatabase db;

  void createDB();
  bool enabled();
};

#endif  // SRC_LIB_LOCAL_H_
