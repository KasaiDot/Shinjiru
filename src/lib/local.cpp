/* Copyright 2015 Kazakuri */

#include "./local.h"

#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>

#include "../settings.h"
#include "../api/user.h"

Local::Local(QObject *parent) : QObject(parent) {
  db = QSqlDatabase::addDatabase("QSQLITE");
  db.setDatabaseName(qApp->applicationDirPath() + "/user.db");

  if (!db.open()) {
    qCritical() << tr("Could not open database connection");
    qDebug() << db.lastError().text();
  }

  createDB();
}

Local::~Local() {
  if (db.isOpen()) db.close();
}

bool Local::enabled() {
  Settings s;
  return s.getValue(Settings::LocalDBEnabled, D_LOCAL_DB_ENABLED).toBool();
}

void Local::createDB() {
  if (!enabled()) return;
  QFile init(":/shinjiru.sql");
  init.open(QFile::ReadOnly);

  QString data = init.readAll();
  QStringList statements = data.split(";");

  QSqlQuery result;

  for (QString statement : statements) {
    if (statement.trimmed().isEmpty()) continue;

    if (!result.exec(statement)) {
      qDebug() << result.lastError();
      qDebug() << "at:" << statement;
    }
  }
}

void Local::loadUser() {
  if (!enabled()) return;

  QSqlQuery result = db.exec("SELECT * FROM users");

  if (result.first()) {
    QString image_url = result.value("image_url").toString();

    User::sharedUser()->setDisplayName(result.value("display_name").toString());
    User::sharedUser()->setScoreType(result.value("score_type").toInt());
    User::sharedUser()->setProfileImageURL(image_url);
    User::sharedUser()->setTitleLanguage(
        result.value("title_language").toString());
    User::sharedUser()->setAnimeTime(result.value("anime_time").toInt());
    User::sharedUser()->setNotificationCount(
        result.value("notifications").toInt());
    User::sharedUser()->setCustomLists(
        result.value("custom_lists").toString().split(","));
    User::sharedUser()->setUserImage(result.value("image_data").toByteArray(),
                                     image_url.split(".").last());
  } else {
    qDebug() << result.lastError();
  }
}

void Local::saveUser() {
  if (!enabled()) return;
  QSqlQuery query(db);

  query.prepare(
      "REPLACE INTO users (display_name, score_type, image_url, "
      "title_language, anime_time, notifications, custom_lists, "
      "image_data) VALUES "
      "(:display_name, :score_type, :image_url, :title_language, "
      ":anime_time, :notifications, :custom_lists, :image_data)");

  QString display_name = User::sharedUser()->displayName();
  QString score_type = QString::number(User::sharedUser()->scoreType());
  QString image_url = User::sharedUser()->profileImageURL();
  QString title_language = User::sharedUser()->titleLanguage();
  QString anime_time = QString::number(User::sharedUser()->animeTime());
  QString notifications =
      QString::number(User::sharedUser()->notificationCount());
  QVariantList c_lists = User::sharedUser()->customLists();
  QStringList custom_lists;

  for (QVariant v : c_lists) {
    custom_lists.append(v.toString());
  }

  QByteArray img_data;
  QBuffer buffer(&img_data);
  User::sharedUser()->userImage().save(
      &buffer, image_url.split(".").last().toLatin1().data());

  query.bindValue(":display_name", display_name);
  query.bindValue(":score_type", score_type);
  query.bindValue(":image_url", image_url);
  query.bindValue(":title_language", title_language);
  query.bindValue(":anime_time", anime_time);
  query.bindValue(":notifications", notifications);
  query.bindValue(":custom_lists", custom_lists.join(","));
  query.bindValue(":image_data", img_data);

  query.exec();

  if (query.lastError().isValid()) {
    qDebug() << query.lastError();
    qDebug() << "at:" << query.lastQuery();
  }
}

bool Local::loadAnime(Anime *anime) {
  if (!enabled()) return false;
  QSqlQuery result =
      db.exec("SELECT * FROM anime WHERE id = " + anime->getID());

  if (result.first()) {
    Settings s;
    int t = s.getValue(Settings::LocalDBTime, D_LOCAL_DB_TIME).toInt();
    if (QDateTime::fromString(result.value("last_updated").toString()) <
        QDateTime::currentDateTimeUtc().addDays(-t)) {
      return false;
    }

    if (result.value("airing_status").toString() == "currently airing")
      return false;

    anime->setRomajiTitle(result.value("romaji_title").toString());
    anime->setJapaneseTitle(result.value("japanese_title").toString());
    anime->setEnglishTitle(result.value("english_title").toString());
    anime->setType(result.value("type").toString());
    anime->setAiringStatus(result.value("airing_status").toString());
    anime->setEpisodeCount(result.value("episode_count").toInt());
    anime->setAverageScore(result.value("average_score").toString());
    anime->setSynopsis(result.value("synopsis").toString());
    anime->setCoverURL(QUrl(result.value("cover_url").toString()));
    anime->setCoverImageData(result.value("cover_image").toByteArray(), "JPG");
    anime->setDuration(result.value("duration").toInt());
    // anime->setCountdown(QDateTime::currentDateTimeUtc());
    anime->setNextEpisode(result.value("next_episode_number").toInt());

    result =
        db.exec("SELECT * FROM synonyms WHERE anime_id = " + anime->getID());

    while (result.next()) {
      anime->addSynonym(result.value("synonym").toString());
    }

    return true;
  }

  return false;
}

void Local::saveAnime(Anime *anime) {
  if (!enabled()) return;
  QSqlQuery query(db);

  query.prepare(
      "REPLACE INTO anime (id, romaji_title, japanese_title, "
      "english_title, type, airing_status, episode_count, "
      "average_score, synopsis, cover_url, cover_image, duration, "
      "next_episode_date, next_episode_number, last_updated)"
      " VALUES "
      "(:id, :romaji_title, :japanese_title, :english_title, :type, "
      ":airing_status, :episode_count, :average_score, :synopsis, "
      ":cover_url, :cover_image, :duration, :next_episode_date, "
      ":next_episode_number, :last_updated)");

  QByteArray img_data;
  QBuffer buffer(&img_data);
  anime->getCoverImage().save(&buffer, "JPG");

  query.bindValue(":id", anime->getID());
  query.bindValue(":romaji_title", anime->getRomajiTitle());
  query.bindValue(":japanese_title", anime->getJapaneseTitle());
  query.bindValue(":english_title", anime->getEnglishTitle());
  query.bindValue(":type", anime->getType());
  query.bindValue(":airing_status", anime->getAiringStatus());
  query.bindValue(":episode_count", anime->getEpisodeCount());
  query.bindValue(":average_score", anime->getAverageScore());
  query.bindValue(":synopsis", anime->getSynopsis());
  query.bindValue(":cover_url", anime->getCoverURL().toDisplayString());
  query.bindValue(":cover_image", img_data);
  query.bindValue(":duration", anime->getDuration());
  query.bindValue(":next_episode_date", QVariant(QVariant::String));
  query.bindValue(":next_episode_number", anime->getNextEpisode());
  query.bindValue(":last_updated", QDateTime::currentDateTimeUtc().toString());

  query.exec();

  if (query.lastError().isValid()) {
    qDebug() << query.lastError();
    qDebug() << query.executedQuery();
  }
}
