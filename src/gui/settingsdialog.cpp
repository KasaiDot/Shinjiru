/* Copyright 2015 Kazakuri */

#include "./settingsdialog.h"

#include <QDesktopServices>
#include <QUrl>
#include <QDir>
#include <QMessageBox>

#include "./ui_settingsdialog.h"
#include "../settings.h"
#include "../lib/skinmanager.h"
#include "../api/anime.h"
#include "../api/user.h"

#ifdef Q_OS_WIN
const QString winkey =
    "HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run";
#endif

const QChar sp = QChar(0x202F);
const QString seperator = QString(sp + QString(" - "));

SettingsDialog::SettingsDialog(QWidget *parent)
    : QDialog(parent), ui(new Ui::SettingsDialog) {
  ui->setupUi(this);
  setAttribute(Qt::WA_DeleteOnClose);
  setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

  ui->settingsTypeList->item(0)->setIcon(QIcon(QPixmap(
      SkinManager::sharedManager()->get(SkinManager::ApplicationIcon))));
  ui->settingsTypeList->item(1)->setIcon(QIcon(
      QPixmap(SkinManager::sharedManager()->get(SkinManager::AnimeListIcon))));
  ui->settingsTypeList->item(2)->setIcon(QIcon(QPixmap(
      SkinManager::sharedManager()->get(SkinManager::RecognitionIcon))));
  ui->settingsTypeList->item(3)->setIcon(QIcon(
      QPixmap(SkinManager::sharedManager()->get(SkinManager::TorrentsIcon))));
  ui->settingsTypeList->item(4)->setIcon(QIcon(
      QPixmap(SkinManager::sharedManager()->get(SkinManager::AdvancedIcon))));

  connect(ui->settingsTypeList, &QListWidget::currentRowChanged,
          [&](int row) {  // NOLINT
            ui->settingsTypeTabs->setCurrentIndex(row);
          });
  connect(ui->disconnectButton, SIGNAL(clicked(bool)), SLOT(resetAPI()));
  connect(ui->defaultButton, SIGNAL(clicked(bool)), SLOT(defaultSettings()));
  connect(ui->openSkinsFolderButton, &QPushButton::clicked, [&]() {  // NOLINT
    QDesktopServices::openUrl(QUrl(qApp->applicationDirPath() + "/data/skin/"));
  });
  connect(ui->newTitle, &QPushButton::clicked, [&]() {  // NOLINT
    ui->smartTitleList->addItem("**New**");
    ui->smartTitleList->setCurrentRow(ui->smartTitleList->count() - 1);
  });

  connect(ui->deleteTitle, &QPushButton::clicked, [&]() {  // NOLINT
    QListWidgetItem *current = ui->smartTitleList->currentItem();

    if (current != nullptr) {
      int row = ui->smartTitleList->row(current);
      delete current;

      ui->smartTitleList->setCurrentRow(qMax(row - 1, 0));
    }
  });
  connect(ui->aliasLineEdit, SIGNAL(textEdited(QString)),
          SLOT(updateSmartTitleName()));
  connect(ui->titleComboBox, SIGNAL(currentIndexChanged(int)),
          SLOT(updateSmartTitleName()));
  connect(ui->offsetSpinBox, SIGNAL(valueChanged(int)),
          SLOT(updateSmartTitleName()));

  connect(ui->smartTitleList, &QListWidget::currentItemChanged,
          [&]() {  // NOLINT
            if (ui->smartTitleList->currentItem() == nullptr) return;

            QStringList text =
                ui->smartTitleList->currentItem()->text().split(seperator);

            ui->aliasLineEdit->setText(text.at(0));

            ui->offsetSpinBox->disconnect(this);
            ui->offsetSpinBox->setValue(text.last().toInt());
            connect(ui->offsetSpinBox, SIGNAL(valueChanged(int)),
                    SLOT(updateSmartTitleName()));

            if (text.length() > 2) {
              ui->titleComboBox->disconnect(this);
              ui->titleComboBox->setCurrentText(text.at(1) + seperator +
                                                text.at(2));
              connect(ui->titleComboBox, SIGNAL(currentIndexChanged(int)),
                      SLOT(updateSmartTitleName()));
            }

            if (text.length() > 3)
              ui->offsetSpinBox->setValue(text.at(3).toInt());
          });

  connect(ui->advancedValue, &QLineEdit::textEdited, [&]() {  // NOLINT
    int row = ui->advancedTable->currentRow();
    ui->advancedTable->item(row, 1)->setText(ui->advancedValue->text());
  });

  connect(ui->advancedTable, &QTableWidget::currentItemChanged,
          [&]() {  // NOLINT
            int row = ui->advancedTable->currentRow();
            ui->advancedValue->setText(ui->advancedTable->item(row, 1)->text());
          });

  connect(ui->torrentRuleList, &QListWidget::currentItemChanged,
          [&]() {  // NOLINT
            if (ui->torrentRuleList->count() == 0) return;

            if (!old_key.isEmpty() && torrent_rules.contains(old_key)) {
              torrent_rules.remove(old_key);
              QListWidgetItem *item =
                  ui->torrentRuleList->findItems(current_rule, Qt::MatchExactly)
                      .at(0);

              QJsonObject new_rule;
              QString key;

              if (ui->basicBox->isChecked()) {
                key = ui->animeTitleLineEdit->text();

                new_rule.insert("rule_type", "basic");
                new_rule.insert("anime", ui->animeTitleLineEdit->text());
                new_rule.insert("subgroup", ui->subGroupLineEdit->text());
                new_rule.insert("resolution",
                                ui->animeResolutionComboBox->currentText());
              } else {
                key = ui->fileRegexLineEdit->text();

                new_rule.insert("rule_type", "advanced");
                new_rule.insert("regex", ui->fileRegexLineEdit->text());
              }

              if (key.isEmpty()) key = "**New**";

              torrent_rules.insert(key, new_rule);
              item->setText(key);
            }

            if (ui->torrentRuleList->count() == 0) {
              ui->animeTitleLineEdit->setText("");
              ui->subGroupLineEdit->setText("");
              ui->animeResolutionComboBox->setCurrentText("720p");
              ui->fileRegexLineEdit->setText("");
            }

            if (ui->torrentRuleList->currentItem() == nullptr) {
              old_key = "";
              current_rule = "";
              return;
            }

            QString key = ui->torrentRuleList->currentItem()->text();
            QJsonObject rule = torrent_rules.value(key).toObject();

            QString rule_type = rule.value("rule_type").toString();

            if (rule_type == "basic") {
              ui->basicBox->setChecked(true);
              ui->advancedBox->setChecked(false);

              ui->animeTitleLineEdit->setText(rule.value("anime").toString());
              ui->subGroupLineEdit->setText(rule.value("subgroup").toString());
              ui->animeResolutionComboBox->setCurrentText(
                  rule.value("resolution").toString());
              ui->fileRegexLineEdit->setText("");
            } else {
              ui->basicBox->setChecked(true);
              ui->advancedBox->setChecked(false);

              ui->fileRegexLineEdit->setText(rule.value("regex").toString());
              ui->animeTitleLineEdit->setText("");
              ui->subGroupLineEdit->setText("");
              ui->animeResolutionComboBox->setCurrentText("720p");
            }

            current_rule = key;
            old_key = key;
          });

  connect(ui->animeTitleLineEdit, &QLineEdit::textEdited, [&]() {  // NOLINT
    if (ui->torrentRuleList->currentItem() == nullptr) return;

    ui->torrentRuleList->currentItem()->setText(ui->animeTitleLineEdit->text());
    current_rule = ui->animeTitleLineEdit->text();
  });

  connect(ui->fileRegexLineEdit, &QLineEdit::textEdited, [&]() {  // NOLINT
    ui->torrentRuleList->currentItem()->setText(ui->fileRegexLineEdit->text());
  });

  connect(ui->basicBox, SIGNAL(toggled(bool)), SLOT(toggleBasic(bool)));
  connect(ui->advancedBox, SIGNAL(toggled(bool)), SLOT(toggleAdvanced(bool)));
  connect(ui->newTorrentRule, &QPushButton::clicked, [&]() {  // NOLINT
    saveTorrentRules();
    ui->torrentRuleList->addItem("**New**");
    old_key = "**New**";

    ui->torrentRuleList->setCurrentRow(ui->torrentRuleList->count() - 1);

    ui->fileRegexLineEdit->setText("");
    ui->animeTitleLineEdit->setText("");
    ui->animeResolutionComboBox->setCurrentText("720p");
    ui->subGroupLineEdit->setText("");
  });

  connect(ui->deleteTorrentRule, &QPushButton::clicked, [&]() {  // NOLINT
    QListWidgetItem *current = ui->torrentRuleList->currentItem();

    if (current != nullptr) {
      int row = ui->torrentRuleList->row(current);
      torrent_rules.remove(old_key);

      delete current;

      ui->torrentRuleList->setCurrentRow(
          qMin(qMax(row, 0), ui->torrentRuleList->count() - 1));
    }
  });

  ui->torrentTabs->setCurrentIndex(0);
  ui->settingsTypeTabs->tabBar()->hide();
  ui->settingsTypeTabs->setCurrentIndex(0);
  ui->smartTitleList->setCurrentRow(0);
  updateSmartTitleName();

  awesome = new QtAwesome(qApp);
  awesome->initFontAwesome();

  QVariantMap black;
  black.insert("color", QColor(0, 0, 0));
  black.insert("color-active", QColor(0, 0, 0));
  black.insert("color-disabled", QColor(0, 0, 0));
  black.insert("color-selected", QColor(0, 0, 0));

  ui->moveDownButton->setIcon(awesome->icon(fa::arrowdown, black));
  ui->moveUpButton->setIcon(awesome->icon(fa::arrowup, black));
  ui->moveUpButton->setText("");
  ui->moveDownButton->setText("");
  ui->advancedTable->resizeColumnToContents(0);
  ui->advancedTable->horizontalHeader()->setSectionResizeMode(
      QHeaderView::Fixed);

  connect(ui->moveUpButton, SIGNAL(clicked()), SLOT(moveUp()));
  connect(ui->moveDownButton, SIGNAL(clicked()), SLOT(moveDown()));

  QFont font = ui->orderListWidget->font();
  font.setCapitalization(QFont::Capitalize);
  ui->orderListWidget->setFont(font);

  loadSettings();
}

SettingsDialog::~SettingsDialog() {
  delete ui;
  delete awesome;
}

void SettingsDialog::accept() {
  applySettings();
  done(QDialog::Accepted);
}

void SettingsDialog::loadSettings() {
  Settings s;

  /* --- APPLICATION SETTINGS --- */

  // Startup
  bool start_on_boot =
      s.getValue(Settings::StartOnBoot, D_START_ON_BOOT).toBool();
  bool check_for_updates =
      s.getValue(Settings::CheckUpdates, D_CHECK_FOR_UPDATES).toBool();
  bool start_minimized =
      s.getValue(Settings::StartMinimized, D_START_MINIMIZED).toBool();

  if (start_on_boot) {
#ifdef Q_OS_WIN
    QSettings reg(winkey, QSettings::NativeFormat);
    QString path = reg.value("Shinjiru", QString("")).toString();
    if (path.isEmpty()) start_on_boot = false;
#endif
  }

  ui->startOnBootCheck->setChecked(start_on_boot);
  ui->checkforUpdatesCheck->setChecked(check_for_updates);
  ui->startMinimizedCheck->setChecked(start_minimized);

  // Update Settings
  QString update_stream =
      s.getValue(Settings::ReleaseStream, D_UPDATE_STREAM).toString();

  ui->updateStreamComboBox->setCurrentText(update_stream);

  // System Tray
  bool minimize_to_tray =
      s.getValue(Settings::MinimizeToTray, D_MINIMIZE_TO_TRAY).toBool();
  bool close_to_tray =
      s.getValue(Settings::CloseToTray, D_CLOSE_TO_TRAY).toBool();

  ui->minimizeToTrayCheck->setChecked(minimize_to_tray);
  ui->closeToTrayCheck->setChecked(close_to_tray);

  /* --- ANIME LIST --- */
  bool store_local =
      s.getValue(Settings::LocalDBEnabled, D_LOCAL_DB_ENABLED).toBool();

  ui->storeLocalchk->setChecked(store_local);

  int cache_time = s.getValue(Settings::LocalDBTime, D_LOCAL_DB_TIME).toInt();
  ui->localDaysLineEdit->setText(QString::number(cache_time));

  QStringList list_order =
      s.getValue(Settings::ListOrder, QStringList()).toStringList();
  ui->orderListWidget->addItems(list_order);

  /* --- RECOGNITION SETTINGS --- */

  // General Recognition Settings
  bool ear = s.getValue(Settings::AnimeRecognitionEnabled, D_ANIME_RECOGNITION)
                 .toBool();
  ui->EARCheck->setChecked(ear);

  // Notification Settings
  bool detect_notify =
      s.getValue(Settings::AnimeDetectNotify, D_DETECT_NOTIFY).toBool();
  bool update_notify =
      s.getValue(Settings::AnimeUpdateNotify, D_UPDATE_NOTIFY).toBool();

  ui->detectNotifyCheck->setChecked(detect_notify);
  ui->updateNotifyCheck->setChecked(update_notify);

  // Update Settings
  int update_delay =
      s.getValue(Settings::AutoUpdateDelay, D_UPDATE_DELAY).toInt();

  ui->updateDelaySpinBox->setValue(update_delay);

  // Smart Titles
  loadSmartTitles();

  /* --- TORRENT SETTINGS --- */

  // Automation
  bool enable_torrents =
      s.getValue(Settings::TorrentsEnabled, D_TORRENTS_ENABLED).toBool();
  int refresh_interval =
      s.getValue(Settings::TorrentRefreshTime, DT_REFRESH_INTERVAL).toInt();
  bool auto_download = s.getValue(Settings::AutoDownload, DT_DOWNLOAD).toBool();
  bool auto_notify = s.getValue(Settings::AutoNotify, DT_NOTIFY).toBool();

  ui->torrentCheck->setChecked(enable_torrents);
  ui->refreshIntervalSpinBox->setValue(refresh_interval);
  ui->downloadRadio->setChecked(auto_download);
  ui->notifyRadio->setChecked(auto_notify);

  // Torrent Rules
  loadTorrentRules();

  /* --- ADVANCED SETTINGS --- */
  int user_refresh_time =
      s.getValue(Settings::UserRefreshTime, D_USER_REFRESH_TIME).toInt();
  QString enable_all_column =
      s.getValue(Settings::AllColumnEnabled, D_ALL_COLUMN_ENABLED).toString();

  setAdvancedSetting("User_Refresh_Time", QString::number(user_refresh_time));
  setAdvancedSetting("Enable_All_Column", enable_all_column);
}

void SettingsDialog::setAdvancedSetting(QString s, QString value) {
  QTableWidgetItem *item =
      ui->advancedTable->findItems(s, Qt::MatchExactly).at(0);
  int row = ui->advancedTable->row(item);

  ui->advancedTable->setItem(row, 1, new QTableWidgetItem(value));
}

QString SettingsDialog::getAdvancedSetting(QString s) {
  QTableWidgetItem *item =
      ui->advancedTable->findItems(s, Qt::MatchExactly).at(0);
  int row = ui->advancedTable->row(item);

  return ui->advancedTable->item(row, 1)->text();
}

void SettingsDialog::defaultSettings() {
  if (QMessageBox::No ==
      QMessageBox::question(this, "Shinjiru",
                            tr("Are you sure you want to reset the settings to "
                               "their default values?"),
                            QMessageBox::Yes | QMessageBox::No))
    return;

  /* --- APPLICATION SETTINGS --- */

  // Startup
  ui->startOnBootCheck->setChecked(D_START_ON_BOOT);
  ui->checkforUpdatesCheck->setChecked(D_CHECK_FOR_UPDATES);
  ui->startMinimizedCheck->setChecked(D_START_MINIMIZED);

  // Update Settings
  ui->updateStreamComboBox->setCurrentText(D_UPDATE_STREAM);

  // System Tray
  ui->minimizeToTrayCheck->setChecked(D_MINIMIZE_TO_TRAY);
  ui->closeToTrayCheck->setChecked(D_CLOSE_TO_TRAY);

  /* --- ANIME LIST --- */
  ui->storeLocalchk->setChecked(D_LOCAL_DB_ENABLED);
  ui->localDaysLineEdit->setText(QString::number(D_LOCAL_DB_TIME));

  ui->orderListWidget->clear();

  /* --- RECOGNITION SETTINGS --- */

  // General Recognition Settings
  ui->EARCheck->setChecked(D_ANIME_RECOGNITION);

  // Notification Settings
  ui->detectNotifyCheck->setChecked(D_DETECT_NOTIFY);
  ui->updateNotifyCheck->setChecked(D_UPDATE_NOTIFY);

  // Update Settings
  ui->updateDelaySpinBox->setValue(D_UPDATE_DELAY);

  // Smart Titles
  ui->smartTitleList->clear();
  ui->titleComboBox->setCurrentIndex(0);
  ui->aliasLineEdit->setText("");
  ui->offsetSpinBox->setValue(0);

  saveSmartTitles();

  /* --- TORRENT SETTINGS --- */

  // Automation
  ui->torrentCheck->setChecked(D_TORRENTS_ENABLED);
  ui->refreshIntervalSpinBox->setValue(DT_REFRESH_INTERVAL);
  ui->downloadRadio->setChecked(DT_DOWNLOAD);
  ui->notifyRadio->setChecked(DT_NOTIFY);

  // Torrent Rules
  ui->torrentRuleList->clear();
  ui->animeTitleLineEdit->setText("");
  ui->animeResolutionComboBox->setCurrentText("720p");
  ui->subGroupLineEdit->setText("");
  ui->fileRegexLineEdit->setText("");

  saveTorrentRules();

  /* --- ADVANCED SETTINGS --- */
  setAdvancedSetting("User_Refresh_Time", D_USER_REFRESH_TIME);
  setAdvancedSetting("Enable_All_Column", D_ALL_COLUMN_ENABLED);

  applySettings();
}

void SettingsDialog::applySettings() {
  Settings s;

  /* --- APPLICATION SETTINGS --- */

  // Startup
  bool start_on_boot = ui->startOnBootCheck->isChecked();
  bool check_for_updates = ui->checkforUpdatesCheck->isChecked();
  bool start_minimized = ui->startMinimizedCheck->isChecked();

  s.setValue(Settings::StartOnBoot, start_on_boot);
  s.setValue(Settings::CheckUpdates, check_for_updates);
  s.setValue(Settings::StartMinimized, start_minimized);

  if (start_on_boot) {
#ifdef Q_OS_WIN
    QSettings reg(winkey, QSettings::NativeFormat);
    reg.setValue("Shinjiru",
                 "\"" + qApp->applicationFilePath().replace("/", "\\") + "\"");
#endif
  } else {
#ifdef Q_OS_WIN
    QSettings reg(winkey, QSettings::NativeFormat);
    reg.remove("Shinjiru");
#endif
  }

  // Update Settings
  QString update_stream = ui->updateStreamComboBox->currentText();

  s.setValue(Settings::ReleaseStream, update_stream);

  // System Tray
  bool minimize_to_tray = ui->minimizeToTrayCheck->isChecked();
  bool close_to_tray = ui->closeToTrayCheck->isChecked();

  s.setValue(Settings::MinimizeToTray, minimize_to_tray);
  s.setValue(Settings::CloseToTray, close_to_tray);

  /* --- ANIME LIST --- */
  bool local_store = ui->storeLocalchk->isChecked();
  s.setValue(Settings::LocalDBEnabled, local_store);

  int cache_time = ui->localDaysLineEdit->text().toInt();
  s.setValue(Settings::LocalDBTime, cache_time);

  QStringList list_order;

  for (int i = 0; i < ui->orderListWidget->count(); i++) {
    list_order << ui->orderListWidget->item(i)->text();
  }

  s.setValue(Settings::ListOrder, list_order);

  /* --- RECOGNITION SETTINGS --- */

  // General Recognition Settings
  bool ear = ui->EARCheck->isChecked();
  s.setValue(Settings::AnimeRecognitionEnabled, ear);

  // Notification Settings
  bool detect_notify = ui->detectNotifyCheck->isChecked();
  bool update_notify = ui->updateNotifyCheck->isChecked();

  s.setValue(Settings::AnimeDetectNotify, detect_notify);
  s.setValue(Settings::AnimeUpdateNotify, update_notify);

  // Update Settings
  int update_delay = ui->updateDelaySpinBox->value();
  s.setValue(Settings::AutoUpdateDelay, update_delay);

  // Smart Titles
  saveSmartTitles();

  /* --- TORRENT SETTINGS --- */

  // Automation
  bool enable_torrents = ui->torrentCheck->isChecked();
  int refresh_interval = ui->refreshIntervalSpinBox->value();
  bool auto_download = ui->downloadRadio->isChecked();
  bool auto_notify = ui->notifyRadio->isChecked();

  s.setValue(Settings::TorrentsEnabled, enable_torrents);
  s.setValue(Settings::TorrentRefreshTime, refresh_interval);
  s.setValue(Settings::AutoDownload, auto_download);
  s.setValue(Settings::AutoNotify, auto_notify);

  // Torrent Rules
  saveTorrentRules();

  /* --- ADVANCED SETTINGS --- */
  s.setValue(Settings::UserRefreshTime,
             getAdvancedSetting("User_Refresh_Time").toInt());

  s.setValue(Settings::AllColumnEnabled,
             getAdvancedSetting("Enable_All_Column"));
}

void SettingsDialog::moveUp() {
  if (ui->orderListWidget->selectedItems().count() == 1) {
    int row =
        ui->orderListWidget->row(ui->orderListWidget->selectedItems().at(0));
    if (row != 0) {
      ui->orderListWidget->insertItem(
          row - 1, ui->orderListWidget->takeItem(row)->text());
      ui->orderListWidget->setCurrentRow(row - 1);
    }
  }
}

void SettingsDialog::moveDown() {
  if (ui->orderListWidget->selectedItems().count() == 1) {
    int row =
        ui->orderListWidget->row(ui->orderListWidget->selectedItems().at(0));
    if (row != ui->orderListWidget->count()) {
      ui->orderListWidget->insertItem(
          row + 1, ui->orderListWidget->takeItem(row)->text());
      ui->orderListWidget->setCurrentRow(row + 1);
    }
  }
}

void SettingsDialog::loadTorrentRules() {
  QFile tor_rule_file(QCoreApplication::applicationDirPath() + "/rules.json");
  tor_rule_file.open(QFile::ReadWrite);

  QByteArray data = tor_rule_file.readAll();

  torrent_rules = QJsonDocument::fromJson(data).object();
  ui->torrentRuleList->addItems(torrent_rules.keys());
}

void SettingsDialog::saveTorrentRules() {
  if (!old_key.isEmpty()) {
    torrent_rules.remove(old_key);
    QListWidgetItem *item =
        ui->torrentRuleList->findItems(current_rule, Qt::MatchExactly).at(0);
    int index = ui->torrentRuleList->row(item);

    QJsonObject new_rule;
    QString key;

    if (ui->basicBox->isChecked()) {
      key = ui->animeTitleLineEdit->text();

      new_rule.insert("rule_type", "basic");
      new_rule.insert("anime", ui->animeTitleLineEdit->text());
      new_rule.insert("subgroup", ui->subGroupLineEdit->text());
      new_rule.insert("resolution", ui->animeResolutionComboBox->currentText());
    } else {
      key = ui->fileRegexLineEdit->text();

      new_rule.insert("rule_type", "advanced");
      new_rule.insert("regex", ui->fileRegexLineEdit->text());
    }

    torrent_rules.insert(key, new_rule);
    delete item;
    ui->torrentRuleList->insertItem(index, key);
  }

  QFile tor_rule_file(QCoreApplication::applicationDirPath() + "/rules.json");
  tor_rule_file.open(QFile::WriteOnly);

  tor_rule_file.write(QJsonDocument(torrent_rules).toJson());
}

void SettingsDialog::loadSmartTitles() {
  QList<Anime *> list = User::sharedUser()->getAnimeList();

  qSort(list.begin(), list.end(), [](Anime *&s1, Anime *&s2) {  // NOLINT
    return s1->getTitle() < s2->getTitle();
  });

  for (Anime *a : list) {
    ui->titleComboBox->addItem(a->getTitle() + seperator + a->getID());
  }

  QFile smart_file(QCoreApplication::applicationDirPath() + "/relations.json");
  if (!smart_file.open(QFile::ReadOnly)) return;

  QJsonArray relations = QJsonDocument::fromJson(smart_file.readAll()).array();

  for (QJsonValue v : relations) {
    QJsonObject relation = v.toObject();

    QString id = relation.value("id").toString("0");
    QString custom = relation.value("custom").toString();
    QString title = relation.value("title").toString();
    int offset = relation.value("offset").toString().toInt(0);

    ui->smartTitleList->addItem(custom + seperator + title + seperator + id +
                                seperator + QString::number(offset));
  }
}

void SettingsDialog::saveSmartTitles() {
  QFile f(QApplication::applicationDirPath() + "/relations.json");
  f.open(QFile::WriteOnly);

  QJsonArray arr;

  for (int i = 0; i < ui->smartTitleList->count(); i++) {
    QStringList data = ui->smartTitleList->item(i)->text().split(seperator);

    if (data.length() < 3) continue;

    QJsonObject o;

    o.insert("id", data.at(2));
    o.insert("title", data.at(1));
    o.insert("custom", data.at(0));
    o.insert("offset", data.at(3));

    arr.append(o);
  }

  f.write(QJsonDocument(arr).toJson());
}
void SettingsDialog::updateSmartTitleName() {
  QString lineText = ui->aliasLineEdit->text() + seperator +
                     ui->titleComboBox->currentText() + seperator +
                     QString::number(ui->offsetSpinBox->value());

  if (ui->smartTitleList->currentItem() != 0)
    ui->smartTitleList->currentItem()->setText(lineText);
}

void SettingsDialog::showSmartTitles() {
  this->show();
  ui->settingsTypeTabs->setCurrentIndex(2);
  ui->settingsTypeList->setCurrentRow(2);
  ui->recognitionTab->setCurrentIndex(1);

  if (ui->smartTitleList->count() > 0) ui->smartTitleList->setCurrentRow(0);
}

void SettingsDialog::showTorrentRules(QString title, QString sub, QString res,
                                      QString file) {
  this->show();
  ui->settingsTypeTabs->setCurrentIndex(3);
  ui->settingsTypeList->setCurrentRow(3);
  ui->torrentTabs->setCurrentIndex(1);
  ui->newTorrentRule->click();
  ui->torrentRuleList->setCurrentRow(ui->torrentRuleList->count() - 1);

  ui->animeTitleLineEdit->setText(title);
  ui->animeResolutionComboBox->setCurrentText(res);
  ui->subGroupLineEdit->setText(sub);
  ui->fileRegexLineEdit->setText(file);

  ui->torrentRuleList->currentItem()->setText(ui->animeTitleLineEdit->text());
}

void SettingsDialog::toggleBasic(bool en) {
  ui->basicBox->setChecked(en);
  ui->advancedBox->setChecked(!en);
}

void SettingsDialog::toggleAdvanced(bool en) {
  ui->basicBox->setChecked(!en);
  ui->advancedBox->setChecked(en);
}

void SettingsDialog::resetAPI() {
  Settings s;
  API::sharedAPI()->sharedAniListAPI()->setAuthorizationCode("");
  API::sharedAPI()->sharedAniListAPI()->setAuthorizationPin("");

  s.setValue(Settings::AniListAccess, "");
  s.setValue(Settings::AniListExpires, QDateTime::currentDateTimeUtc());
  s.setValue(Settings::AniListRefresh, "");

  QProcess::startDetached(QApplication::applicationFilePath());
  exit(0);
}
