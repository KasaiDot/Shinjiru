/* Copyright 2015 Kazakuri */

#ifndef SRC_GUI_PROGRESSBOX_H_
#define SRC_GUI_PROGRESSBOX_H_

#include <QSpinBox>
#include <QWidget>
#include <QEvent>
#include <QWheelEvent>

class ProgressBox : public QSpinBox {
  Q_OBJECT

 public:
  explicit ProgressBox(QWidget *parent = 0) : QSpinBox(parent) {}

 protected:
  void wheelEvent(QWheelEvent *e) {
    if (focusPolicy() == Qt::WheelFocus) {
      e->accept();
    } else {
      e->ignore();
    }
  }

  void focusInEvent(QFocusEvent *e) {
    Q_UNUSED(e)
    setFocusPolicy(Qt::WheelFocus);
  }

  void focusOutEvent(QFocusEvent *e) {
    Q_UNUSED(e)
    setFocusPolicy(Qt::StrongFocus);
  }
};

#endif  // SRC_GUI_PROGRESSBOX_H_
