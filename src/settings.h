/* Copyright 2015 Kazakuri */

#ifndef SRC_SETTINGS_H_
#define SRC_SETTINGS_H_

#include <QObject>
#include <QSettings>

#include "./version.h"

/* --- Define a bunch of default values for user settings --- */
#define D_START_ON_BOOT      FALSE
#define D_CHECK_FOR_UPDATES  FALSE
#define D_START_MINIMIZED    FALSE
#define D_UPDATE_STREAM      "Stable"
#define D_MINIMIZE_TO_TRAY   FALSE
#define D_CLOSE_TO_TRAY      TRUE
#define D_ANIME_RECOGNITION  TRUE
#define D_DETECT_NOTIFY      TRUE
#define D_UPDATE_NOTIFY      TRUE
#define D_UPDATE_DELAY       120
#define D_TORRENTS_ENABLED   TRUE
#define DT_REFRESH_INTERVAL  900
#define DT_DOWNLOAD          TRUE
#define DT_NOTIFY            FALSE
#define D_USER_REFRESH_TIME  "1200"
#define D_LOCAL_DB_ENABLED   TRUE
#define D_LOCAL_DB_TIME      7
#define D_ALL_COLUMN_ENABLED "1"

class Settings : public QObject {
  Q_OBJECT

 public:
  explicit Settings(QObject *parent = 0);
  void setValue(int, QVariant);
  QVariant getValue(int, QVariant);

  enum SettingTypes {
    AnimeRecognitionEnabled,
    TorrentRefreshTime,
    TorrentRSSURL,
    DefaultRuleType,
    AniListExpires,
    AniListRefresh,
    AniListAccess,
    AutoUpdateDelay,
    StartOnBoot,
    MinimizeToTray,
    CloseToTray,
    ListOrder,
    DownloadCount,
    RuleCount,
    ReleaseStream,
    CheckUpdates,
    StartMinimized,
    AnimeDetectNotify,
    AnimeUpdateNotify,
    TorrentsEnabled,
    AutoDownload,
    AutoNotify,
    UserRefreshTime,
    LocalDBEnabled,
    LocalDBTime,
    AllColumnEnabled
  };

 private:
  QSettings settings;
  QString parseEnum(int setting);
};

#endif  // SRC_SETTINGS_H_
