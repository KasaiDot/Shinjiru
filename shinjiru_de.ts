<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>API</name>
    <message>
        <location filename="src/api/api.cpp" line="74"/>
        <source>Authorization Pin Request</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>APIWebView</name>
    <message>
        <location filename="src/gui/apiwebview.ui" line="14"/>
        <source>API Access Request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/apiwebview.ui" line="33"/>
        <source>about:blank</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <location filename="src/gui/about.ui" line="26"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/about.ui" line="38"/>
        <source>Shinjiru</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/about.ui" line="45"/>
        <source>Version 0.0.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/about.ui" line="59"/>
        <source>Created by Kazakuri</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/about.ui" line="71"/>
        <source>Special thanks to: Linas Valiukas, Eren Okka, &amp; Rick Blommers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/about.ui" line="85"/>
        <source>Shinjiru is anime list management tool built with Qt designed to automatically update your progress to AniList and act an desktop manager for AniList.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/about.cpp" line="24"/>
        <source>Version </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AiringAnime</name>
    <message>
        <location filename="src/gui/airinganime.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AnimePanel</name>
    <message>
        <location filename="src/gui/animepanel.ui" line="20"/>
        <source>Anime Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="36"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="68"/>
        <source>Synonyms: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="107"/>
        <source>Details:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="160"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="167"/>
        <source>TV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="174"/>
        <source>Average Score:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="181"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="188"/>
        <source>Episodes:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="195"/>
        <source>24</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="202"/>
        <source>Airing Status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="209"/>
        <source>Airing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="216"/>
        <source>Synopsis:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="264"/>
        <source>Episodes Watched:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="271"/>
        <source>Score: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="278"/>
        <source>Status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="285"/>
        <source>Notes:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="293"/>
        <source>Watching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="298"/>
        <source>Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="303"/>
        <source>On-hold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="308"/>
        <source>Dropped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="313"/>
        <source>Plan to Watch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/animepanel.ui" line="341"/>
        <source>Rewatched: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BrowseAnime</name>
    <message>
        <location filename="src/gui/browseanime.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FvUpdateDownloadProgress</name>
    <message>
        <location filename="lib/fervor/fvupdatedownloadprogress.ui" line="23"/>
        <source>FvUpdateDownloadProgress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdatedownloadprogress.ui" line="61"/>
        <source>Downloading Update...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FvUpdateWindow</name>
    <message>
        <location filename="lib/fervor/fvupdatewindow.ui" line="14"/>
        <source>Software Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdatewindow.ui" line="31"/>
        <source>A new version of %1 is available!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdatewindow.ui" line="38"/>
        <source>%1 %2 is now available - you have %3. Would you like to download it now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdatewindow.ui" line="51"/>
        <source>Release Notes:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdatewindow.ui" line="64"/>
        <source>about:blank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdatewindow.ui" line="77"/>
        <source>Skip This Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdatewindow.ui" line="97"/>
        <source>Remind Me Later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdatewindow.ui" line="104"/>
        <source>Install Update</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FvUpdater</name>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="275"/>
        <source>Moving file %1 to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="310"/>
        <source>Error: Unable to open zip archive %1 for unzipping: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="326"/>
        <source>Error: Unable to retrieve fileInfo about the file to extract: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="335"/>
        <source>Error: Unable to open file %1 for unzipping: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="342"/>
        <source>Error: Unable to retrieve zipped filename to unzip from %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="354"/>
        <source>Error: Unable to unzip file %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="359"/>
        <source>Error: Have read all available bytes, but pointer still does not show EOF: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="366"/>
        <source>Error: Unable to close zipped file %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="374"/>
        <source>Error: Unable to close zip archive file %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="517"/>
        <source>Feed download failed: %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="636"/>
        <source>Feed parsing failed: %1 %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="664"/>
        <source>Feed error: &quot;release notes&quot; link is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="673"/>
        <source>Feed error: invalid &quot;release notes&quot; link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="677"/>
        <source>Feed error: invalid &quot;enclosure&quot; with the download link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="685"/>
        <source>No updates were found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="733"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="753"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="lib/fervor/fvupdater.cpp" line="814"/>
        <source>SSL fingerprint check: The url %1 is not a ssl connection!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Local</name>
    <message>
        <location filename="src/lib/local.cpp" line="16"/>
        <source>Could not open database connection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="src/gui/mainwindow.ui" line="21"/>
        <source>Shinjiru</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="67"/>
        <location filename="src/gui/mainwindow.ui" line="140"/>
        <source>Anime List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="80"/>
        <location filename="src/gui/mainwindow.ui" line="434"/>
        <source>Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="93"/>
        <location filename="src/gui/mainwindow.ui" line="398"/>
        <location filename="src/gui/mainwindow.ui" line="1008"/>
        <source>Currently Airing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="100"/>
        <location filename="src/gui/mainwindow.ui" line="807"/>
        <location filename="src/gui/mainwindow.ui" line="858"/>
        <source>Browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="113"/>
        <location filename="src/gui/mainwindow.ui" line="234"/>
        <source>Torrents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="204"/>
        <source>Filter anime or search AniList...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="269"/>
        <source>Refresh (0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="289"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;span&gt;An anime is irrelevant if it is not airing or it is not on your list.&lt;/span&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="292"/>
        <source>Hide irrelevant anime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="314"/>
        <source>Filter anime...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="364"/>
        <source>Anime Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="369"/>
        <source>Ep.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="374"/>
        <source>Subgroup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="379"/>
        <source>Video Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="384"/>
        <source>File Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="389"/>
        <source>Direct Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="450"/>
        <source>AniList Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="462"/>
        <source>Days Watched: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="469"/>
        <location filename="src/gui/mainwindow.ui" line="476"/>
        <location filename="src/gui/mainwindow.ui" line="497"/>
        <location filename="src/gui/mainwindow.ui" line="511"/>
        <location filename="src/gui/mainwindow.ui" line="525"/>
        <location filename="src/gui/mainwindow.ui" line="539"/>
        <location filename="src/gui/mainwindow.ui" line="553"/>
        <location filename="src/gui/mainwindow.ui" line="567"/>
        <location filename="src/gui/mainwindow.ui" line="588"/>
        <location filename="src/gui/mainwindow.ui" line="602"/>
        <location filename="src/gui/mainwindow.ui" line="616"/>
        <location filename="src/gui/mainwindow.ui" line="630"/>
        <location filename="src/gui/mainwindow.ui" line="701"/>
        <location filename="src/gui/mainwindow.ui" line="736"/>
        <location filename="src/gui/mainwindow.ui" line="743"/>
        <location filename="src/gui/mainwindow.ui" line="750"/>
        <location filename="src/gui/mainwindow.ui" line="757"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="490"/>
        <source>Watching:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="504"/>
        <source>Completed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="518"/>
        <source>On Hold:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="532"/>
        <source>Dropped:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="546"/>
        <source>Plan to Watch: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="560"/>
        <source>Total Entries:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="581"/>
        <source>Mean Score:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="595"/>
        <source>Median Score:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="609"/>
        <source>Mode Score:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="623"/>
        <source>Score Deviation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="637"/>
        <source>Episodes Watched:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="682"/>
        <source>Shinjiru Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="694"/>
        <source>Application Uptime: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="708"/>
        <source>Torrents Downloaded From Rules:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="715"/>
        <source>Torrents Downloaded:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="722"/>
        <source>Torrents Downloaded Since Launch:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="729"/>
        <source>Torrents Downloaded From Rules Since Launch:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="880"/>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="900"/>
        <source>Season</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="913"/>
        <source>Winter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="918"/>
        <source>Spring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="923"/>
        <source>Summer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="928"/>
        <source>Fall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="940"/>
        <location filename="src/gui/userlisthelper.cpp" line="284"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="953"/>
        <source>TV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="958"/>
        <source>Movie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="963"/>
        <source>Special</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="968"/>
        <source>OVA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="973"/>
        <source>ONA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="978"/>
        <source>TV Short</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="990"/>
        <location filename="src/gui/userlisthelper.cpp" line="282"/>
        <location filename="src/gui/userlisthelper.cpp" line="309"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1003"/>
        <source>Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1013"/>
        <source>Finished Airing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1018"/>
        <source>Not Yet Aired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1076"/>
        <source>Genres</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1152"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1159"/>
        <source>&amp;AniList</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1168"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1178"/>
        <location filename="src/gui/mainwindow.ui" line="1240"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1197"/>
        <source>E&amp;xit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1202"/>
        <source>&amp;Refresh List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1207"/>
        <source>View &amp;Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1212"/>
        <source>View &amp;Dashboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1217"/>
        <source>View Anime &amp;List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1225"/>
        <source>Enable Anime &amp;Recognition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1230"/>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1235"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1245"/>
        <source>&amp;Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1250"/>
        <source>&amp;Anime Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1255"/>
        <source>Export List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.ui" line="1260"/>
        <source>Anime Recognition Rules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="267"/>
        <source>Error: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="485"/>
        <source>Unknown anime.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="586"/>
        <source>Updating %1 to episode %2 in %3 seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="617"/>
        <source>%1 updated to episode %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="621"/>
        <source>%1 marked as completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="625"/>
        <source>%1 was not successfully updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="638"/>
        <source>Refresh (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="641"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="673"/>
        <source> days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="675"/>
        <source> day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="681"/>
        <source> hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="683"/>
        <source> hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="690"/>
        <source> minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="692"/>
        <source> minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="700"/>
        <source> second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="702"/>
        <source> seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="713"/>
        <source>Save File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="715"/>
        <source>Json File (*.json)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/mainwindow.cpp" line="726"/>
        <source>Error %d: File could not be written to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/statshelper.cpp" line="7"/>
        <source>Updating statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/statshelper.cpp" line="113"/>
        <source>%1 days, %2 hours, and %3 minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/torrentshelper.cpp" line="377"/>
        <source>New matching torrents found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/torrentshelper.cpp" line="392"/>
        <source>%1 has started downloading.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/trayhelper.cpp" line="41"/>
        <source>&amp;Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/trayhelper.cpp" line="44"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/trayhelper.cpp" line="49"/>
        <source>&amp;Anime Recognition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/trayhelper.cpp" line="55"/>
        <source>&amp;Cancel Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userhelper.cpp" line="11"/>
        <source>Loading User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userhelper.cpp" line="35"/>
        <source>User Loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="15"/>
        <source>Downloading User List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="28"/>
        <source>Parsing User List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="280"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="280"/>
        <source>Episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="280"/>
        <source>Score</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="306"/>
        <source>Open Anime Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="308"/>
        <source>Increment Progress by 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="310"/>
        <source>Delete Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="311"/>
        <source>Custom Lists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="313"/>
        <source>Watching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="314"/>
        <source>On Hold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="316"/>
        <source>Plan to Watch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="317"/>
        <source>Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="318"/>
        <source>Dropped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="321"/>
        <source>Hide Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="554"/>
        <source>Nothing found, click here to open a search box.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/userlisthelper.cpp" line="576"/>
        <source>Anime list empty, add some using the anime browse tab!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="src/main.cpp" line="110"/>
        <source>Enable debug output</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuaZipFilePrivate</name>
    <message>
        <location filename="lib/fervor/quazip/quazipfile.cpp" line="217"/>
        <source>ZIP/UNZIP API error %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchPanel</name>
    <message>
        <location filename="src/gui/searchpanel.ui" line="14"/>
        <source>Anime Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/searchpanel.ui" line="25"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/searchpanel.ui" line="65"/>
        <source>about:blank</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="14"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="32"/>
        <location filename="src/gui/settingsdialog.ui" line="64"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="37"/>
        <location filename="src/gui/settingsdialog.ui" line="192"/>
        <source>Anime List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="42"/>
        <location filename="src/gui/settingsdialog.ui" line="263"/>
        <source>Recognition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="47"/>
        <location filename="src/gui/settingsdialog.ui" line="485"/>
        <source>Torrents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="52"/>
        <location filename="src/gui/settingsdialog.ui" line="693"/>
        <location filename="src/gui/settingsdialog.ui" line="727"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="76"/>
        <source>Application Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="83"/>
        <source>Startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="89"/>
        <source>Start on Boot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="96"/>
        <source>Check for Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="103"/>
        <source>Start Minimized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="113"/>
        <location filename="src/gui/settingsdialog.ui" line="331"/>
        <source>Update Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="124"/>
        <source>Update Stream:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="132"/>
        <source>Stable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="137"/>
        <source>Beta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="150"/>
        <source>System Tray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="156"/>
        <source>Minimize to Tray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="163"/>
        <source>Close to Tray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="198"/>
        <source>List Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="215"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="222"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="275"/>
        <source>Anime Recognition Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="286"/>
        <source>Recognition Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="292"/>
        <source>General Recognition Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="298"/>
        <source>Enable Anime Recognition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="308"/>
        <source>Notification Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="314"/>
        <source>Notify When Media is Detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="321"/>
        <source>Notify When List is Updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="342"/>
        <source>Update Delay:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="375"/>
        <source>Smart Title Rules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="390"/>
        <source>Title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="417"/>
        <source>Alias:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="437"/>
        <source>Offset:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="464"/>
        <location filename="src/gui/settingsdialog.ui" line="612"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="471"/>
        <location filename="src/gui/settingsdialog.ui" line="619"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="497"/>
        <source>Torrent Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="508"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="514"/>
        <source>Automation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="520"/>
        <source>Check for New Releases Automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="532"/>
        <source>Refresh Interval:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="548"/>
        <source>On New Matches</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="554"/>
        <source>Notify Me</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="561"/>
        <source>Download Automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="588"/>
        <source>Download Rules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="628"/>
        <source>Basic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="639"/>
        <source>Anime Title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="649"/>
        <source>Anime Resolution:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="660"/>
        <source>480p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="665"/>
        <source>720p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="670"/>
        <source>1080p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="678"/>
        <source>Sub Group:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="707"/>
        <source>File Regex:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="739"/>
        <source>Advanced Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="746"/>
        <source>Warning: Only modify if you know what you&apos;re doing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="778"/>
        <source>New Row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="783"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="788"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="793"/>
        <source>User_Refresh_Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="808"/>
        <source>Value:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="828"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="835"/>
        <source>Open Skins Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.ui" line="855"/>
        <source>Restore Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/gui/settingsdialog.cpp" line="381"/>
        <source>Are you sure you want to reset the settings to their default values?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
