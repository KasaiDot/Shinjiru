CREATE TABLE IF NOT EXISTS `anime` (
  `id` int(11) PRIMARY KEY NOT NULL,
  `romaji_title` text NOT NULL,
  `japanese_title` text NOT NULL,
  `english_title` text NOT NULL,
  `type` text NOT NULL,
  `airing_status` text NOT NULL,
  `episode_count` int(11) NOT NULL,
  `average_score` tinytext NOT NULL,
  `synopsis` mediumtext NOT NULL,
  `cover_url` text NOT NULL,
  `cover_image` blob NOT NULL,
  `duration` int(11) NOT NULL,
  `next_episode_date` datetime,
  `next_episode_number` int(11),
  `last_updated` datetime NOT NULL
);

CREATE TABLE IF NOT EXISTS `synonyms` (
  `id` integer PRIMARY KEY NOT NULL,
  `anime_id` int(11) NOT NULL,
  `synonym` text NOT NULL
);

CREATE TABLE IF NOT EXISTS `users` (
  `display_name` text PRIMARY KEY NOT NULL,
  `score_type` int(11) NOT NULL,
  `image_url` text NOT NULL,
  `image_data` blob NOT NULL,
  `title_language` text NOT NULL,
  `anime_time` int(11) NOT NULL,
  `notifications` int(11) NOT NULL,
  `custom_lists` text NOT NULL
);

CREATE TABLE IF NOT EXISTS `user_anime` (
  `id` integer PRIMARY KEY NOT NULL,
  `anime_id` int(11) NOT NULL,
  `progress` int(11) NOT NULL,
  `score` text NOT NULL,
  `notes` mediumtext NOT NULL,
  `rewatch` int(11) NOT NULL,
  `status` tinytext NOT NULL,
  `custom_lists` int(11) NOT NULL
);
